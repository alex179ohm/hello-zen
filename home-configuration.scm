;; This "home-environment" file can be passed to 'guix home reconfigure'
;; to reproduce the content of your profile.  This is "symbolic": it only
;; specifies package names.  To reproduce the exact same profile, you also
;; need to capture the channels being used, as returned by "guix describe".
;; See the "Replicating Guix" section in the manual.

(use-modules (gnu home)
             (gnu packages)
             (gnu services)
             (guix gexp)
             (gnu home services shells))

(home-environment
  ;; Below is the list of packages that will show up in your
  ;; Home profile, under ~/.guix-home/profile.
  (packages (specifications->packages (list "glibc-locales" "hello-zen@0.1")))

  ;; Below is the list of Home services.  To search for available
  ;; services, run 'guix home search KEYWORD' in a terminal.
  (services
   (list (service home-bash-service-type
                  (home-bash-configuration
                   (aliases '((".." . "cd ..") ("..." . "cd ../..")
                              ("...." . "cd ../../..")
                              ("big" . "expac -H M '\\''%m\\t%n'\\'' | sort -h | nl")
                              ("bw" . "/home/alex/Applications/bitwarden/bw")
                              ("cat" . "bat --style header,rule,snip,changes --theme ansi")
                              ("cb" . "cargo build")
                              ("cbr" . "cargo build --release")
                              ("ccov" . "cargo llvm-cov nextest --html --open")
                              ("cleanup" . "sudo pacman -Rns (pacman -Qtdq)")
                              ("cr" . "cargo run")
                              ("ct" . "cargo nextest")
                              ("ctr" . "cargo nextest run")
                              ("dir" . "dir --color=auto")
                              ("egrep" . "egrep --color=auto")
                              ("fgrep" . "fgrep --color=auto")
                              ("fixpacman" . "sudo rm /var/lib/pacman/db.lck")
                              ("gitpkg" . "pacman -Q | grep -i \"\\-git\" | wc -l")
                              ("grep" . "grep --color=auto")
                              ("grubup" . "sudo update-grub")
                              ("hw" . "hwinfo --short")
                              ("ip" . "ip -color")
                              ("jctl" . "journalctl -p 3 -xb")
                              ("l." . "exa -a | egrep '\\''^\\.'\\''")
                              ("la" . "exa -a --color=always --group-directories-first --icons")
                              ("ll" . "exa -l --color=always --group-directories-first --icons")
                              ("ls" . "exa -al --color=always --group-directories-first --icons")
                              ("lt" . "exa -T --color=always --group-directories-first --icons --git-ignore")
                              ("lta" . "exa -aT --color=always --group-directories-first --icons")
                              ("ltg" . "exa -T --color=always --group-directories-first --icons")
                              ("mirror" . "sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist")
                              ("mirrora" . "sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist")
                              ("mirrord" . "sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist")
                              ("mirrors" . "sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist")
                              ("psmem" . "ps auxf | sort -nr -k 4")
                              ("psmem10" . "ps auxf | sort -nr -k 4 | head -10")
                              ("rip" . "expac --timefmt='\\''%Y-%m-%d %T'\\'' '\\''%l\\t%n %v'\\'' | sort | tail -200 | nl")
                              ("rmpkg" . "sudo pacman -Rdd")
                              ("tarnow" . "tar -acf ")
                              ("tb" . "nc termbin.com 9999")
                              ("untar" . "tar -xvf ")
                              ("upd" . "/usr/bin/update")
                              ("vdir" . "vdir --color=auto")
                              ("wget" . "wget -c ")
                              ("yay" . "paru")))
                   (bashrc (list (local-file
                                  "/home/alex/Fun/RustRoma/guix_rust/.bashrc"
                                  "bashrc")))
                   (bash-profile (list (local-file
                                        "/home/alex/Fun/RustRoma/guix_rust/.bash_profile"
                                        "bash_profile")))
                   (bash-logout (list (local-file
                                       "/home/alex/Fun/RustRoma/guix_rust/.bash_logout"
                                       "bash_logout"))))))))
