#+Title: Guix + Rust
#+Subtitle: The Zen of Rust Packaging
#+Author: Alessandro Cresto Miseroglio
#+Email: alessandro.cresto.miseroglio@gmail.com

#+OPTIONS: timestamp:nil toc:nil num:nil
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_REVEAL_JS_VERSION: 4
#+REVEAL_EXTRA_CSS: logo.css
#+REVEAL_TITLE_SLIDE: <h1>%t<h1><br><h3>%s<h3>
#+REVEAL_THEME: beige

* Chi sono
- Imprenditore/Investitore
- Appassionato di Programmazione
- Lavoro come Freelance (Ogni tanto)
- Rust Lover
- Utente Emacs (Org, Ledger, Magit, Coding)

* Perche' parliamo di Rust Packaging?
 - Mettere in produzione applicazioni Rust a volte non e' semplice (vedi il talk di tglman qui al RustRoma)
 - Le Dipendenze esterne non sono semplici da gestire
 - Dev-Ops Rapido e Indolore
 - Eliminazione di Docker (non abbiamo bisogno di una Distro su una Distro)

* Ambienti riproducibili per applicazioni
- NixOS (Nix language)
- Riff (scritto in Rust, Nix based, specifico per Rust)
- Guix (scritto in guile scheme, Nix based)

* Perche Guix?
- Guile e' un **semplice** e **potente** linguaggio di scripting
- Non e' specifico per Rust
- E' basato su Nix
- Simple deploy
- Riproducibili e Configurabili Ambienti di sviluppo (Home shell)

* Guix Packaging steps
** Crea un tuo channel

#+begin_src lisp
;; ~/.config/guix/channels.scm
(cons* (channel
       (channel
       (name 'ohm)
       (url "https://gitlab.com/alex179ohm/ohm-channel.git")
       (branch "main")
       (introduction
        (make-channel-introduction
         "9136d823d4ff6f0961a700a361ee798a42662985"
         (openpgp-fingerprint
          "1C42 FE7F C908 97AC BBEF  37EF 3AFF F1AA 9CE9 A03B"))))
       %default-channels)
#+end_src

[[https:gitlab.com/alex179ohm/ohm-channel][Esempio Channel]]

** Genera l'hash del package

#+begin_src text
$ guix download https://gitlab.com/alex179ohm/hello-zen

Starting download of /tmp/guix-file.OeKOUJ
From https://gitlab.com/alex179ohm/hello-zen...
 hello-zen        3.0MiB/s 00:00 | 99KiB transferred
/gnu/store/r8s82dc1pfjps23191ig6i7rbhb4pd2w-hello-zen
13pl8j463v3j01shar8l3g33bbkyavlf4i0jy3di4qa0khqil0li
#+end_src

** Definisci il package

[[file:~/Guix/ohm-channel/ohm/packages/hello-zen.scm][File Esempio Package]]

** Update del profilo

#+begin_src bash
guix pull
#+end_src

- Esegue il check di autenticazione dei commit sui channels
- Esegue il check per i nuovi pacchetti
- Compila il nuovo profilo

** Installa il pacchetto
#+begin_src bash
guix install hello-zen
#+end_src

* Riproduci la tua home

#+REVEAL_OPTIONS: :frag t
- Importa la tua home

 #+begin_src bash
guix home import <project_path>
 #+end_src

- Genera la tua home

#+begin_src bash
guix home reconfigure home-configuration.scm
#+end_src

- Lancia la tua applicazione

 #+begin_src bash
guix home container hello-configuration.scm -- hello-zen
 #+end_src

* Crea un ambiente di sviluppo riproducibile

** Aggiunti il file manifest.scm al tuo progetto
#+begin_src lisp
(specification->manifest
  '(
    ;; aggiungi i tuoi pacchetti
    "rust-cargo@0.53.0"
    "emacs"))
#+end_src

** In Remoto

#+begin_src bash
git clone <your project>
#+end_src

#+begin_src bash
guix shell --pure
#+end_src

* Puoi creare la tua distro

- Descrivi il tuo sistema
 [[https://guix.gnu.org/manual/en/html_node/Using-the-Configuration-System.html][Guix System Doc]]
 [[https:guix.gnu.org/blog/2019/managing-servers-with-gnu-guix-a-tutorial/][Official Guix Tutorial]]
- Crea l'immagine
- Deploy

** Supporta differenti tipi di output

#+begin_src text
 ❯ guix system image --list-image-types
The available image types are:

   - rock64-raw
   - pinebook-pro-raw
   - pine64-raw
   - novena-raw
   - hurd-qcow2
   - hurd-raw
   - uncompressed-iso9660
   - tarball
   - docker
   - efi32-raw
   - efi-raw
   - raw-with-offset
   - qcow2
   - iso9660
   - wsl2
#+end_src

** Supporta Docker
#+begin_src bash
guix system docker-image example_system.scm

image_id="$(docker load < guix-system-docker-image.tar.gz)"
container_id="$(docker create $image_id)"
docker start $container_id
#+end_src

* Contributing

[[https://guix.gnu.org/manual/en/html_node/Contributing.html][Guix Contributing Doc]]

[[https://git.savannah.gnu.org/cgit/guix.git/tree/gnu/packages/crates-io.scm][Guix Rust Packages]]

* Prossimo Talk

Guix + Rust
The zen of Web Rust, packaging and deploy

* Grazie Mille

Qualche domanda?
