#!/usr/bin/env bash
#
# ~/.bashrc
#

eval "$(starship init bash)"

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# change directory just typing its name
shopt -s autocd

## Useful aliases
# Replace ls with exa
alias ls='exa -al --color=always --group-directories-first --icons'             # preferred listing
alias la='exa -a --color=always --group-directories-first --icons'              # all files and dirs
alias ll='exa -l --color=always --group-directories-first --icons'              # long format
alias lt='exa -T --color=always --group-directories-first --icons --git-ignore' # tree listing
alias ltg='exa -T --color=always --group-directories-first --icons'             # tree listing
alias lta='exa -aT --color=always --group-directories-first --icons'            # tree listing
alias l.="exa -a | egrep '^\.'"                                                 # show only dotfiles
alias ip="ip -color"

# Replace some more things with better alternatives
alias cat='bat --style header,rule,snip,changes --theme ansi'
[ ! -x /usr/bin/yay ] && [ -x /usr/bin/paru ] && alias yay='paru'

# Common use
alias grubup="sudo update-grub"
alias fixpacman="sudo rm /var/lib/pacman/db.lck"
alias tarnow='tar -acf '
alias untar='tar -xvf '
alias wget='wget -c '
alias rmpkg="sudo pacman -Rdd"
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias upd='/usr/bin/update'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias dir='dir --color=auto'
alias vdir='vdir --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias hw='hwinfo --short'                          # Hardware Info
alias big="expac -H M '%m\t%n' | sort -h | nl"     # Sort installed packages according to size in MB
alias gitpkg='pacman -Q | grep -i "\-git" | wc -l' # List amount of -git packages

# Get fastest mirrors
alias mirror="sudo reflector -f 30 -l 30 --number 10 --verbose --save /etc/pacman.d/mirrorlist"
alias mirrord="sudo reflector --latest 50 --number 20 --sort delay --save /etc/pacman.d/mirrorlist"
alias mirrors="sudo reflector --latest 50 --number 20 --sort score --save /etc/pacman.d/mirrorlist"
alias mirrora="sudo reflector --latest 50 --number 20 --sort age --save /etc/pacman.d/mirrorlist"

# Help people new to Arch
alias tb='nc termbin.com 9999'

# Cleanup orphaned packages
alias cleanup='sudo pacman -Rns (pacman -Qtdq)'

# Get the error messages from journalctl
alias jctl="journalctl -p 3 -xb"

# Recent installed packages
alias rip="expac --timefmt='%Y-%m-%d %T' '%l\t%n %v' | sort | tail -200 | nl"

# cargo alias
alias cr="cargo run"
alias cb="cargo build"
alias cbr="cargo build --release"
alias ct="cargo nextest"
alias ctr="cargo nextest run"
alias ccov="cargo llvm-cov nextest --html --open"

# local bin path
export PATH="$PATH:$HOME/.local/bin"

# cargo
export CARGO_UNSTABLE_SPARSE_REGISTRY=true
. "$HOME/.cargo/env"

export WASMTIME_HOME="$HOME/.wasmtime"

export PATH="$WASMTIME_HOME/bin:$PATH"
export PNPM_HOME="/home/alex/.local/share/pnpm"
export PATH="$PNPM_HOME:$PATH"

[ -f "/home/alex/.ghcup/env" ] && source "/home/alex/.ghcup/env" # ghcup-env

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/alex/Applications/google-cloud-sdk/path.bash.inc' ]; then . '/home/alex/Applications/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/alex/Applications/google-cloud-sdk/completion.bash.inc' ]; then . '/home/alex/Applications/google-cloud-sdk/completion.bash.inc'; fi

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# skip mirrorlist update on /usr/bin/garuda-update aka update
export SKIP_MIRRORLIST=1

# ghcup (haskell)
export PATH="$HOME/.ghcup/bin::$PATH"

# golang
export GO11MODULE="one"
export GOPATH="$HOME/Devel/go"
export PATH="/usr/local/go/bin:$GOPATH/bin:$PATH"
export PATH="$HOME/.pub-cache/bin:$PATH"

# Vs Code
#export PATH="$HOME/Applications/VSCode-linux-x64/bin:$PATH"

# Android SDK
export ANDROID_HOME="$HOME/Android/Sdk"
# Android NDK
export ANDROID_NDK="$ANDROID_HOME/ndk/25.0.8775105"
export ANDROID_NDK_HOME="$ANDROID_HOME/ndk"

# OpenCl rocm amd
export AMDGPU_TARGETS="gfx902"

# doom emacs
export PATH="$HOME/.emacs.d/bin:$PATH"

# guix load path
export GUIX_PROFILE="/home/alex/.config/guix/current"
#export GUIX_PROFILE="$HOME/.guix-profile"
#export GUIX_LOCPATH="$GUIX_PROFILE/lib/locale"
export GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
#source "$GUIX_PROFILE/etc/profile"
source "${HOME}/Devel/guix/etc/completion/bash/guix"
source "$HOME/Devel/guix/etc/completion/bash/guix-daemon"

# flutter
export PATH="$PATH:$HOME/Devel/flutter/bin"
export CHROME_EXECUTABLE="google-chrome-stable"

# bitwarden cli
alias bw="$HOME/Applications/bitwarden/bw"
export BW_SESSION="T+MFqAG309ykphFi+iFNG/ijlAlpwoesw2rdk/f2fbog92yyysf99CGHY5N/sVeMFVAxDlMIUtTmdG2MLwfylg=="
